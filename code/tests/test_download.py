#%%writefile /Users/peerherholz/Desktop/choice_rtt/code/tests/test_download.py # this is magic you can use in jupyter notebook using the shell

import requests
from zipfile import ZipFile, BadZipFile
from io import BytesIO
import os

def test_download_and_extraction():
    """
    Test downloading a ZIP file from a URL and extracting it to a specified path.

    Args:
    - url (str): URL of the ZIP file to download.
    - extraction_path (str): The filesystem path where the ZIP file contents will be extracted.
    """

    url = 'https://gitlab.com/julia-pfarr/nowaschool/-/raw/main/school/materials/CI_CD/crtt.zip?ref_type=heads'
    extraction_path = '/home/annalinux/NOWA_school/'
    
    # 1. URL Accessibility
    response = requests.head(url)
    assert response.status_code == 200, "URL is not accessible or does not exist"
    #assert 'application/zip' in response.headers['Content-Type'], "URL does not point to a ZIP file"

    # 2. Successful Download
    response = requests.get(url)
    assert response.status_code == 200, "Failed to download the file"

    # 3. Correct File Type and Extraction
    try:
        with ZipFile(BytesIO(response.content)) as zipfile:
            zipfile.extractall(extraction_path)
        assert True  # If extraction succeeds
    except BadZipFile:
        assert False, "Downloaded file is not a valid ZIP archive"

    # 4. Check Extracted Files
    extracted_files = os.listdir(extraction_path)
    assert len(extracted_files) > 0, "No files were extracted"

    print(f"Test passed: Downloaded and extracted ZIP file to {extraction_path}")