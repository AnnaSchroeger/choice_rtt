authors: Anna Schroeger et al
Deary-Liewald reaction time task A target is presented on one of 4 possible locations. Participants have to react with a key response based on the shape of the target (cross, square, plus)
data was collected during a seminar, data is saved for each participant individually as .csv and .txt
a list of all conditions (RTtimeConditions) is saved with the experiment code choiceRTT.psyexp in the code/experiment folder
variables: 

reaction times and identifier for correct responses are saved


contact: annaschroeger@gmail.com


Metadata we need:
events file: onset, duration etc. of stimulus , response_time, trial_type (which condition)
screen specs, operatring system